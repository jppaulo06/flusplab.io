---
title: KernelDevDay - Patrocínio
lang: pt
ref: kerneldevday
layout: default
---

{% include add_multilang.html %}

O KernelDevDay é um evento de programação organizado pelo FLUSP (FLOSS@USP -
Grupo de Extensão do IME/USP) no qual as equipes participantes passam um
determinado período contíguo (aproximadamente 10h) desenvolvendo contribuições
para algum subsistema do Kernel Linux. Para esta primeira edição do
KernelDevDay, buscamos, em especial, contribuir para alguns drivers do
subsistema de sensores (Industrial Input/Output - IIO) que ainda não estão
prontos para serem mesclados na árvore principal do Kernel Linux devido a
razões técnicas.

Nós temos uma missão simples e clara quanto ao nosso objetivo em promover esse
evento: queremos promover a comunidade de contribuidores do Kernel Linux no
Brasil. Dado a experiência do FLUSP em contribuir com diversas comunidades de
software livre, acreditamos que somos capazes de atuar como um agente
catalisador nesse processo de atrair novos desenvolvedores. Também acreditamos
no impacto que o evento traz, contribuindo para a formação de nossos estudantes
e elevando a reputação da comunidade brasileira no cenário internacional. Com
esses objetivos em mente, e visando proporcionar uma ótima experiência aos
participantes, estamos em busca de patrocinadores para nos apoiar na
organização do evento. Prioritariamente, os valores arrecadados serão
investidos em alimentação, impressão de panfletos/banners/crachás e produção de
brindes para os participantes. Oferecemos os seguintes pacotes:

{% include items_with_modals.html %}

A transparência está na raiz dos valores do FLUSP. Por esse motivo, destacamos
que todo valor excedente da organização do evento será revertido para os
seguintes fins:

* Compra de mesas do tipo dobrável, para serem utilizados pelos
  grupos de extensão do IME-USP;
* Financiar as passagens de alguns membros do FLUSP para a Debconf 2019;
* Confecção de camisetas do FLUSP para os membros do grupo e convidados;

Por fim, vamos divulgar o valor que conseguirmos arrecadar e todos os gastos que
tivemos na forma de um relatório no final do evento. Patrocinar este evento é
investir na formação de novos desenvolvedores de software livre do Brasil e
fomentar futuros profissionais a construirem uma sociedade mais colaborativa.

